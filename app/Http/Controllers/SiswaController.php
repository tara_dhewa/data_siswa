<?php

namespace App\Http\Controllers;

use Illuminate\Support\Str;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;
use Symfony\Component\Console\Question\ConfirmationQuestion;

class SiswaController extends Controller
{
    public function index(Request $request)
    {
        if ($request->has('cari')) {
            $data_siswa = \App\Models\Siswa::where('nama_lengkap', 'LIKE', '%' . $request->cari . '%')->get();
        } else {
            $data_siswa = \App\Models\Siswa::All();
        }

        return view('siswa.index', ['data_siswa' => $data_siswa]);
    }
    public function create(Request $request)
    {
        $this->validate($request, [
            'nama_lengkap' => 'required|min:5',
            'email' => 'required | email | unique:users',
            'jenis_kelamin' => 'required',
            'agama' => 'required',
            'avatar' => 'mimes:jpg,png|max:1048'
        ]);
        //insert ke table users
        $user = new \App\Models\User;
        $user->role = 'siswa';
        $user->name = $request->nama_lengkap;
        $user->email = $request->email;
        $user->password = bcrypt('123');
        $user->remember_token = Str::random(60);
        $user->save();

        //insert ke table siswa
        $request->request->add(['user_id' => $user->id]);
        $siswa = \App\Models\Siswa::create($request->all());
        if ($request->hasFile('avatar')) {
            $request->file('avatar')->move('images/', $request->file('avatar')->getClientOriginalName());
            $siswa->avatar = $request->file('avatar')->getClientOriginalName();
            $siswa->save();
        }
        return redirect('siswa')->with('success', 'Data Berhasil Disimpan!');
    }
    public function edit($id)
    {
        $siswa = \App\Models\Siswa::find($id);
        return view('siswa/edit', ['siswa' => $siswa]);
    }
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'avatar' => 'mimes:jpg,png|max:1048'
        ]);
        $siswa = \App\Models\Siswa::find($id);
        $siswa->update($request->all());
        if ($request->hasFile('avatar')) {
            $request->file('avatar')->move('images/', $request->file('avatar')->getClientOriginalName());
            $siswa->avatar = $request->file('avatar')->getClientOriginalName();
            $siswa->save();
        }
        return redirect('siswa')->with('success', 'Data Berhasil Diubah!');
    }
    public function delete($id)
    {
        $siswa = \App\Models\Siswa::find($id);
        $siswa->delete();

        return redirect('siswa')->with('success', 'Data Berhasil Dihapus!');
    }
    public function profile($id)
    {
        $siswa = \App\Models\Siswa::find($id);
        $matapelajaran = \App\Models\Mapel::all();

        //Menyiapkan data nilai untuk chart
        $categories = [];
        $data = [];

        foreach ($matapelajaran as $mp) {
            //foreach ($siswa->mapel as $mp) {                     
            if ($siswa->mapel()->wherePivot('mapel_id', $mp->id)->first()) {
                $categories[] = $mp->nama;
                $data[] = $siswa->mapel()->wherePivot('mapel_id', $mp->id)->first()->pivot->nilai;
            }
        }
        //dd(json_encode($data));
        //dd(json_encode($categories));

        //dd($matapelajaran);
        return view('siswa/profile', ['siswa' => $siswa, 'matapelajaran' => $matapelajaran, 'categories' => $categories, 'data' => $data]);
    }
    public function addnilai(Request $request, $id)
    {
        //dd($request->all());
        $siswa = \App\Models\Siswa::find($id);
        if ($siswa->mapel()->where('mapel_id', $request->mapel)->exists()) {
            return redirect('siswa/' . $id . '/profile')->with('errors', 'Data mata pelajaran sudah ada');
        }
        $siswa->mapel()->attach($request->mapel, ['nilai' => $request->nilai]);

        return redirect('siswa/' . $id . '/profile')->with('success', 'Nilai berhasil dimasukkan!');
    }
    public function deletenilai($idsiswa, $idmapel)
    {
        $siswa = \App\Models\Siswa::find($idsiswa);
        $siswa->mapel()->detach($idmapel);
        return redirect()->back()->with('success', 'Data Nilai berhasil dihapus!');
    }
}
