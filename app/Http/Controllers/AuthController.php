<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth as FacadesAuth;
use SebastianBergmann\CodeCoverage\Report\Html\Dashboard;

class AuthController extends Controller
{
    public function login()
    {
        return view('auths/login');
    }
    public function postlogin(Request $request)
    {
        //dd($request->all());
        if (FacadesAuth::attempt($request->only('email', 'password'))) {
            return redirect('/dashboards');
        }
        return redirect('/login');
    }
    public function logout()
    {
        FacadesAuth::logout();
        return redirect('/login');
    }
}
