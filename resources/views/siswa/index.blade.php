@extends('layout.master')

@section('content')
@include('sweetalert::alert')
<div class="main">
    <div class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title"><strong>Data Siswa</strong></h3>
                            <div class="right">
                                <button type="button" class="btn"><i class="lnr lnr-plus-circle" data-toggle="modal" data-target="#exampleModal">Tambah Data Siswa</i></button>
                            </div>
                        </div>
                        <div class="panel-body">
                            <table class="table table-hover">
                                <thead>
                                    <tr>                                       
                                        <td><strong>Nama Lengkap</strong></td>
                                        <td><strong>Jenis Kelamin</strong></td>
                                        <td><strong>Agama</strong></td>
                                        <td><strong>Alamat</strong></td>
                                        <td><strong>Rata2 Nilai</strong></td>
                                        <td><strong>Aksi</strong></td>                                   
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($data_siswa as $siswa)
                                    <tr>   
                                        <td><a href="/siswa/{{$siswa->id}}/profile">{{$siswa->nama_lengkap}}</a></td>
                                        <td>{{$siswa->jenis_kelamin}}</td>
                                        <td>{{$siswa->agama}}</td>
                                        <td>{{$siswa->alamat}}</td>
                                        <td>{{$siswa->rataRataNilai()}}</td>
                                        <td>
                                            <a href="siswa/{{$siswa->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                                            <a href="siswa/{{$siswa->id}}/delete" class="btn btn-danger btn-sm" onclick="return confirm('Yakin mau dihapus?')">Delete</a>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel"><strong>Tambah Data Siswa</strong></h5>
            </div>
            <div class="modal-body">
                <form action="/siswa/create" method="POST" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="form-group {{$errors->has('nama_lengkap') ? 'has-error' : ''}}">
                        <label for="nama_lengkap" class="form-label"><strong>Nama Lengkap</strong></label>
                        <input name="nama_lengkap" type="text" class="form-control" id="nama_lengkap" aria-describedby="emailHelp" placeholder="Nama Lengkap" value="{{old('nama_lengkap')}}">
                        @if($errors->has('nama_lengkap'))
                        <span class="help-block">{{$errors->first('nama_lengkap')}}</span>
                        @endif
                    </div>
                    <div class="form-group {{$errors->has('email') ? 'has-error' : ''}}">
                        <label for="email" class="form-label"><strong>Email</strong></label>
                        <input name="email" class="form-control" id="email" aria-describedby="emailHelp" placeholder="Email" value="{{old('email')}}">
                        @if($errors->has('email'))
                        <span class="help-block">{{$errors->first('email')}}</span>
                        @endif
                    </div>
                    <div class="form-group {{$errors->has('jenis_kelamin') ? 'has-error' : ''}}">
                        <label for="exampleFormControlSelect1"><strong>Jenis Kelamin</strong></label>
                        <select name="jenis_kelamin" class="form-control" aria-label="Default select example">
                            <option value="L" {{(old('jenis_kelamin') ==  'L') ? ' selected' :''}}>Laki - laki</option>
                            <option value="P" {{(old('jenis_kelamin') == 'P') ? ' selected' :''}}>Perempuan</option>
                        </select>
                        @if($errors->has('jenis_kelamin'))
                        <span class="help-block">{{$errors->first('jenis_kelamin')}}</span>
                        @endif
                    </div>
                    <div class="form-group {{$errors->has('agama') ? 'has-error' : ''}}">
                        <label for="exampleFormControlSelect1"><strong>Agama</strong></label>
                        <select name="agama" class="form-control" aria-label="Default select example">
                            <option value="Islam" {{(old('agama') == 'Islam') ? ' selected' :''}}>Islam</option>
                            <option value="Kristen" {{(old('agama') == 'Kristen') ? ' selected' :''}}>Kristen</option>
                            <option value="Hindu" {{(old('agama') == 'Hindu') ? ' selected' :''}}>Hindu</option>
                            <option value="Budha" {{(old('agama') == 'Budha') ? ' selected' :''}}>Budha</option>
                        </select>
                        @if($errors->has('agama'))
                        <span class="help-block">{{$errors->first('agama')}}</span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlTextarea1" class="form-label"><strong>Alamat</strong></label>
                        <textarea name="alamat" class="form-control" id="exampleFormControlTextarea1" rows="2">{{old('alamat')}}</textarea>
                    </div>
                    <div class="form-group {{$errors->has('avatar') ? 'has-error' : ''}}">
                        <label class="form-label"><strong>Avatar</strong></label>
                        <input name="avatar" type="file" class="form-control">
                        @if($errors->has('avatar'))
                        <span class="help-block">{{$errors->first('avatar')}}</span>
                        @endif
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Tambah</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@stop