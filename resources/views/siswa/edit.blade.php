@extends('layout.master')
@section('content')
@include('sweetalert::alert')
<div class="main">
    <div class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">Edit Data Siswa</h3>
                        </div>
                        <form action="/siswa/{{$siswa->id}}/update" method="POST" enctype="multipart/form-data">
                            {{csrf_field()}}
                            <div class="panel-body">
                                <label for="nama_lengkap" class="form-label"><strong>Nama Lengkap</strong></label>
                                <input name="nama_lengkap" type="text" class="form-control" id="nama_lengkap" value="{{$siswa->nama_lengkap}}" placeholder="Nama Lengkap">
                                <br>
                                <label for="exampleFormControlSelect1"><strong>Jenis Kelamin</strong></label>
                                <select name="jenis_kelamin" class="form-control">
                                    <option disabled selected>Pilih</option>
                                    <option value="L" @if($siswa->jenis_kelamin == 'L') selected @endif>Laki - laki</option>
                                    <option value="P" @if($siswa->jenis_kelamin == 'P') selected @endif>Perempuan</option>
                                </select>
                                <br>
                                <label for="exampleFormControlSelect1"><strong>Agama</strong></label>
                                <select name="agama" class="form-control">
                                    <option disabled selected>Pilih</option>
                                    <option value="Islam" @if($siswa->agama == 'Islam') selected @endif>Islam</option>
                                    <option value="Kristen" @if($siswa->agama == 'Kristen') selected @endif>Kristen</option>
                                    <option value="Hindu" @if($siswa->agama == 'Hindu') selected @endif>Hindu</option>
                                    <option value="Budha" @if($siswa->agama == 'Budha') selected @endif>Budha</option>
                                </select>
                                <br>
                                <label for="exampleFormControlTextarea1" class="form-label"><strong>Alamat</strong></label>
                                <textarea name="alamat" class="form-control" rows="2">{{$siswa->alamat}}</textarea>
                                <br>
                                <div class="form-group {{$errors->has('avatar') ? 'has-error' : ''}}">
                                    <label class="form-label"><strong>Avatar</strong></label>
                                    <input name="avatar" type="file" class="form-control">
                                    @if($errors->has('avatar'))
                                    <span class="help-block">{{$errors->first('avatar')}}</span>
                                    @endif
                                </div>
                                <button type="submit" class="btn btn-warning">Update</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop