<?php

use App\Http\Controllers\SiswaController;
use GuzzleHttp\Promise\Create;
use Illuminate\Support\Facades\Route;
use RealRashid\SweetAlert\Facades\Alert;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Route::get('/login', 'App\Http\Controllers\AuthController@login')->name('login');
Route::post('/postlogin', 'App\Http\Controllers\AuthController@postlogin');
Route::get('/logout', 'App\Http\Controllers\AuthController@logout');

Route::group(['middleware' => ['auth', 'checkRole:admin']], function () {

    Route::get('siswa', 'App\Http\Controllers\SiswaController@index');
    Route::post('/siswa/create', 'App\Http\Controllers\SiswaController@create');
    Route::get('siswa/{id}/edit', 'App\Http\Controllers\SiswaController@edit');
    Route::post('siswa/{id}/update', 'App\Http\Controllers\SiswaController@update');
    Route::get('siswa/{id}/delete', 'App\Http\Controllers\SiswaController@delete');
    Route::get('siswa/{id}/profile', 'App\Http\Controllers\SiswaController@profile');
    Route::post('/siswa/{id}/addnilai', 'App\Http\Controllers\SiswaController@addnilai');
    Route::get('siswa/{id}/{idmapel}/deletenilai', 'App\Http\Controllers\SiswaController@deletenilai');
    Route::get('/guru/{id}/profile', 'App\Http\Controllers\GuruController@profile');
});

Route::group(['middleware' => ['auth', 'checkRole:admin,siswa']], function () {
    Route::get('/dashboards', 'App\Http\Controllers\DashboardController@index'); //->middleware('auth')
});
