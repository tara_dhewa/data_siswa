-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 07, 2021 at 09:42 AM
-- Server version: 10.4.17-MariaDB
-- PHP Version: 7.4.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `laravel-crud`
--

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `guru`
--

CREATE TABLE `guru` (
  `id` int(11) NOT NULL,
  `nama` varchar(45) NOT NULL,
  `telpon` varchar(15) NOT NULL,
  `alamat` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `update_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `guru`
--

INSERT INTO `guru` (`id`, `nama`, `telpon`, `alamat`, `created_at`, `update_at`) VALUES
(1, 'Budi budian', '0825356655', 'Malang', '2021-02-07 02:54:26', '0000-00-00 00:00:00'),
(2, 'Andi Odang', '08125354685', 'Batu', '2021-02-07 02:54:26', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `mapel`
--

CREATE TABLE `mapel` (
  `id` int(11) NOT NULL,
  `kode` varchar(191) NOT NULL,
  `nama` varchar(191) NOT NULL,
  `semester` varchar(45) NOT NULL,
  `guru_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `update_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `mapel`
--

INSERT INTO `mapel` (`id`, `kode`, `nama`, `semester`, `guru_id`, `created_at`, `update_at`) VALUES
(1, 'M001', 'Matematika', 'Ganjil', 1, '2021-02-01 16:21:08', '0000-00-00 00:00:00'),
(2, 'B001', 'Bahasa Indonesia', 'Ganjil', 2, '2021-02-01 16:21:08', '0000-00-00 00:00:00'),
(3, 'A001', 'Agama Islam', 'Ganjil', 1, '2021-02-02 13:06:08', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `mapel_siswa`
--

CREATE TABLE `mapel_siswa` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `siswa_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mapel_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nilai` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `mapel_siswa`
--

INSERT INTO `mapel_siswa` (`id`, `siswa_id`, `mapel_id`, `nilai`, `created_at`, `updated_at`) VALUES
(2, '5', '2', '82', '2021-02-01 23:40:32', '2021-02-05 01:35:49'),
(5, '5', '3', '75', NULL, NULL),
(6, '14', '3', '70', NULL, NULL),
(7, '14', '1', '65', NULL, NULL),
(8, '14', '2', '70', NULL, NULL),
(9, '6', '2', '75', NULL, '2021-02-06 19:37:05'),
(10, '6', '3', '85', NULL, NULL),
(11, '5', '1', '80', '2021-02-06 20:01:17', '2021-02-06 20:01:17');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2021_01_07_121517_create_siswa_table', 1),
(5, '2021_02_02_063803_create_mapel_siswa_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `siswa`
--

CREATE TABLE `siswa` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `nama_lengkap` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jenis_kelamin` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `agama` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `siswa`
--

INSERT INTO `siswa` (`id`, `user_id`, `nama_lengkap`, `jenis_kelamin`, `agama`, `alamat`, `avatar`, `created_at`, `updated_at`) VALUES
(5, 0, 'Suci susilowati', 'P', 'Islam', 'batu', 'iko.jpg', '2021-01-13 06:31:19', '2021-02-01 08:09:50'),
(6, 0, 'Yolla Septia', 'P', 'Islam', 'gresik', 'tes.jpg', '2021-01-13 06:40:55', '2021-02-01 07:52:29'),
(14, 0, 'Billa', 'P', 'Islam', 'gresik', NULL, '2021-01-13 07:10:09', '2021-01-13 07:10:09');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `role` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `role`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'Tara', 'tara@gmail.com', NULL, '$2y$10$eUOOc.iLqpyrD6qaXj9qjOQ4blUv7PYZlLAzFyeOKXTWXfwrc6gBu', NULL, '2021-01-31 08:21:12', '2021-01-31 08:21:12'),
(2, 'siswa', 'Dewi Sandrina', 'dewi@gmail.com', NULL, '$2y$10$8U2lk4T/Q4JkvydLaIae2eImg86lU113OIlY1VmptYMAI1GcDpfqa', 'VV0LpCyuv8TyWU3sGAqncw2wQtxYfqSSZUSiydshUF4F2lJcyptrEx7o1KHI', '2021-02-01 08:33:51', '2021-02-01 08:33:51'),
(4, 'siswa', 'Sigit Wahyu A', 'sigitwahyu4444@gmail.com', NULL, '$2y$10$cvf6HVIeBPDK5hBfmGOiO.S6AivKpUNjqiTt7.knx0.WLDHAKUXK2', 'tnLxgos76eyv2R5EYXi8eizhxpDtCX8AHM4GTqPTRSAySkBmYKTUfQH6Cniz', '2021-02-01 20:08:18', '2021-02-01 20:08:18'),
(5, 'siswa', 'Tri Afriana', 'triafriana@gmail.com', NULL, '$2y$10$QMy.1s.Pnex0yHTtoe2mPeYBhhVHV8uc/PWPQSuLl/bsXhGxTwi3e', 'eyMvRnsSFmywTGM20S2SVHLgSPf0uaPlsHS2vk5SZhhY9KyvOtRmvuUAqm8W', '2021-02-01 20:16:28', '2021-02-01 20:16:28'),
(6, 'siswa', 'Elvia E', 'elvi@gmail.com', NULL, '$2y$10$mqZOoJPmWhju91UToB0qROM5rri4BVFJAEJ.dT32OPm5D8KunJ7nG', 'SWFr1JACjWvY7vbgnEmbDoMLEJkWkxWWiRgFtkgZw1OOP9NiRzC37F7U5N5A', '2021-02-01 20:20:42', '2021-02-01 20:20:42'),
(7, 'siswa', 'Zavier', 'zavi@gmail.com', NULL, '$2y$10$yYufMXSj/3aF897z0mBTquR6iMpMCz2XopRveakx7PYkqlaE/RqCm', 'Qj9so2hHDUZdEXH03aa4vgLVUsGxBgv1xV5VIZXXCykpVqGlgwklWUe93SYS', '2021-02-01 20:26:13', '2021-02-01 20:26:13'),
(8, 'siswa', 'Budi Pekerti', 'budi@gmail.com', NULL, '$2y$10$v9.MDBK7OYd92wIDt4q8GetC.p1vLjpHJSBIoHA/a1uv6AcfMltBK', '1F4aMHvybRYD4Da2N19sQ1QwmeOc6BhrM6Vq9urqBABPk40Bubtl5GfNzYKk', '2021-02-01 20:30:07', '2021-02-01 20:30:07'),
(9, 'siswa', 'Dedi s', 'dedi@gmail.com', NULL, '$2y$10$swHIQJyhhmExGtGrCAjfyOpGQWOkRdtDj4RbYe99/xFRHdpWoa67q', 'xEIt9ivA3bMXpHZivpkN7MGz2XVvfUUUlKFoL89pEn4tsSCIV3mttP6kFxwp', '2021-02-01 20:35:32', '2021-02-01 20:35:32'),
(10, 'siswa', 'Yessika', 'yessika@gmail.com', NULL, '$2y$10$6xR3EGMFPNmqzvumzKaFb.W70NckcrljgqamNTNYC1X7m/8eT1AUu', 'ugi0mwkRoIFaqOCuSxmSHd5IQsgDhsgDgMyDPJT6x6vmShp9PWLZWz7FJw8H', '2021-02-01 20:59:59', '2021-02-01 20:59:59'),
(11, 'siswa', 'Agum A', 'agum@gmail.com', NULL, '$2y$10$9VGisAswweCo.GjAos6AbeO5YZ9WgWUTXhs0AaMolsMIvSr6YidyC', 'WrORFDr6NF4OAcu4O74fcvwZwKmax67nLilzaeLaIIsjzJ73HCW3RDLyDSLa', '2021-02-01 21:01:36', '2021-02-01 21:01:36'),
(12, 'siswa', 'Yolla Septia', 'yola@gmail.com', NULL, '$2y$10$pY627muY3Z.o5azreIRgNu0yoH6BDGtVVqgkMbD0K5b/HILX/r9F.', 'lGh5KTrG3unbo7dMhaas9tySo2KOTTzbXdqaZordkWo2WRK2UaRAAd70QGSW', '2021-02-01 23:47:50', '2021-02-01 23:47:50'),
(13, 'siswa', 'Diana s', 'diana@gmail.com', NULL, '$2y$10$Fgcgm47HtRcKwL/7zut2seFK/EuBOb/49mL1LmSlm6vlo6TqArbE.', 'W32jIr99yLZB3VyKdy9Hj9POdVffqdqopznzNaTo6pylw5ktTfe52clkSQkl', '2021-02-01 23:52:07', '2021-02-01 23:52:07'),
(14, 'siswa', 'Dini W', 'dini@gmail.com', NULL, '$2y$10$eT3CvSadhZf/WZGR0lg5UunUePKutFP3O6IRDs7/VzoD4nA6LQhS.', 'f28bSjQFV7BMPetqUXNiF1UEzhnzyx7BvUqG6v4Wipz8nPbBmxFfTIaUnZ4l', '2021-02-01 23:54:51', '2021-02-01 23:54:51'),
(15, 'siswa', 'Dian A', 'dian@gmail.com', NULL, '$2y$10$Z7NOQARVjeq7muoLaX7VDeQdETyslXhlp5192fOWnuVRW4UgZn/Xi', '53ziEjXOvI39G98RWo7T9Yz9t6nCmEPO5sVxAB8tWTJ3mgN2G7Ptw194D8Kf', '2021-02-01 23:55:50', '2021-02-01 23:55:50'),
(16, 'siswa', 'Sinta Ayu', 'sinta@gmail.com', NULL, '$2y$10$dYbb/QCxE4JlKzZ652zAMO0iWeD64Wiaxi/a5rcszrYPzLTouIsRy', 'u9em83H4l5a5j3O6Nmz1naVv2ouA5s8Jzw1dc6CDlvbEFadcqTq11A8uESGR', '2021-02-01 23:57:25', '2021-02-01 23:57:25'),
(17, 'siswa', 'rtdrtgdfgdfg', 'edi@gmail.com', NULL, '$2y$10$nrOmDfoW943VasuP0WU3HeQUSS02Vba0X8IMnQejR21XQ2cwau1ka', 'zZt8I4SvMNoV3msZROFEDqrAMqy43lyMy9JBxoSmLsC7gFSfRdkwZ8UvRgZF', '2021-02-01 23:58:12', '2021-02-01 23:58:12'),
(18, 'siswa', 'rtdrtgdfgdfg', 'udi@gmail.com', NULL, '$2y$10$wdZvmpCL5FcpeIfX1G01mu9cJHlqtnomDvahoP9I0LBLzBz6hNmGW', 'hev0WtbzxxTX9juorNc9wmc6frIzZfk3RfQUiLsWWUHmtzvi5TjvlBzCaGc7', '2021-02-01 23:58:54', '2021-02-01 23:58:54');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `guru`
--
ALTER TABLE `guru`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mapel`
--
ALTER TABLE `mapel`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mapel_siswa`
--
ALTER TABLE `mapel_siswa`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `siswa`
--
ALTER TABLE `siswa`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `guru`
--
ALTER TABLE `guru`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `mapel`
--
ALTER TABLE `mapel`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `mapel_siswa`
--
ALTER TABLE `mapel_siswa`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `siswa`
--
ALTER TABLE `siswa`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
